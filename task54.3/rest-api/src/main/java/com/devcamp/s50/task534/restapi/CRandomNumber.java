package com.devcamp.s50.task534.restapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class CRandomNumber {
    @GetMapping("/devcamp-welcome/random-double")
    public String randomeDoubleNumber() {
        double radomDoubleNum = 0;
        for (int i = 1; i < 100; i++) {
            radomDoubleNum = Math.random() * 100;
        }
        return "Random value in double from 1 to 100 " + radomDoubleNum;
    }

    @GetMapping("/devcamp-welcome/random-int")
    public String randomeIntNumber() {

        int radomIntNum = 1 + (int) (Math.random() * ((10 - 1) + 1));
        return "Random value in double from 1 to 100 " + radomIntNum;
    }

}
