package com.devcamp.s50.task534.restapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApiApplication.class, args);
		// Khai báo đối tượng class randomnumber
		CRandomNumber randomNumber = new CRandomNumber();
		// In ra số ngẫu nhiên kiểu doble từ 1 đến 100 ra console
		System.out.println(randomNumber.randomeDoubleNumber());
		// In ra số ngẫu nhiên kiểu int từ 1 đến 10 ra console
		System.out.println(randomNumber.randomeIntNumber());
	}

}
